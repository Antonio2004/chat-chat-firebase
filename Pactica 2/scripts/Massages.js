import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import { getMessaging, getToken  } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-messaging.js";

const firebaseConfig = {
    apiKey: "AIzaSyCIBqKQrUjDmLpW-TKhBr6kSqOODIUO0pU",
    authDomain: "hololensetrr.firebaseapp.com",
    databaseURL: "https://hololensetrr-default-rtdb.firebaseio.com",
    projectId: "hololensetrr",
    storageBucket: "hololensetrr.appspot.com",
    messagingSenderId: "638325347517",
    appId: "1:638325347517:web:6358be55a6ce4cbb8a13d4",
};

const app = initializeApp(firebaseConfig);
const messaging = getMessaging(app);

getToken({vapidKey: BKdetlPsvRmYzigavwj7h4R2wnK1tWrbcaL9IrV-J0yudbmzpex4uaH6_xuG1DJ8LgAR3zY2XHR4-XB8twvKRkw})
.then((currentToken) => {
    if (currentToken) {
      // Send the token to your server and update the UI if necessary
      // ...
    } else {
      // Show permission request UI
      console.log('No registration token available. Request permission to generate one.');
      // ... 
    }
  }).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    // ...
  });
