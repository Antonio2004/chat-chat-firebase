import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import{ getDatabase, ref, set, push, onValue} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js';


const firebaseConfig = {
    apiKey: "AIzaSyCIBqKQrUjDmLpW-TKhBr6kSqOODIUO0pU",
    authDomain: "hololensetrr.firebaseapp.com",
    databaseURL: "https://hololensetrr-default-rtdb.firebaseio.com",
    projectId: "hololensetrr",
    storageBucket: "hololensetrr.appspot.com",
    messagingSenderId: "638325347517",
    appId: "1:638325347517:web:6358be55a6ce4cbb8a13d4",
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);

let textRef = document.getElementById("inputREF");
let botonRef = document.getElementById("uploadREF");
let downLoadButton = document.getElementById("downloadREF");
let labelRef = document.getElementById("DatabaseValue");
let chatRegister = [];

botonRef.addEventListener( "click" ,buttonEvent);
downLoadButton.addEventListener("click", downloadDB);

downloadDB();

function buttonEvent() {

  let informacion = textRef.value;
  if(informacion != ""){
    set(ref(database, 'TestData/'), {
      text: informacion,
    });
  }
};

function downloadDB(){
  onValue(ref(database, 'TestData/text'), (snapshot) => {
    const data = snapshot.val();
    chatRegister = [data, ...chatRegister];

    let newDiv = document.createElement("div");
    let newContent = document.createTextNode(data)
    newDiv.appendChild(newContent);

    labelRef.appendChild(newDiv);
  });
}