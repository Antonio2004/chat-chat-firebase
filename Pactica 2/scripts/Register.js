
/*

Firebase

|   ^
|   |
|   |
|   |
V   |

Base de Datos

|   ^
|   |
|   |
|   |
V   |

Microcontrolador ESP32 / ESP8266
*/ 

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'; //CDN Central Distrubited network
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
  apiKey: "AIzaSyCIBqKQrUjDmLpW-TKhBr6kSqOODIUO0pU",
  authDomain: "hololensetrr.firebaseapp.com",
  projectId: "hololensetrr",
  storageBucket: "hololensetrr.appspot.com",
  messagingSenderId: "638325347517",
  appId: "1:638325347517:web:6358be55a6ce4cbb8a13d4"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth();

let password = document.getElementById("paswordref");
let email = document.getElementById("emailref");
let buttonLogin = document.getElementById("logInButton");

buttonLogin.addEventListener("click", altaUser);

function altaUser (){
	createUserWithEmailAndPassword(auth, email.value, password.value)
  .then((userCredential) => {
    // Signed in
    const user = userCredential.user;
    alert("User created");
    // ...
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    alert("Error");
    // ..
  });
}